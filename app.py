import requests
import json


# access_token=response['access_token']
# url="https://login.microsoftonline.com/botframework.com/oauth2/v2.0/token"
# header={
# 'Host': 'login.microsoftonline.com',
# 'Content-Type': 'application/x-www-form-urlencoded'
# }
# body='grant_type=client_credentials&client_id='+app_id+'&client_secret='+app_password+'&scope=https%3A%2F%2Fapi.botframework.com%2F.default'
#
#
# r=requests.post(url=url,headers=header,data=body)
# a=r.content.decode('utf-8')
# response=json.loads(a)

# A very simple Flask Hello World app for you to get started with...

from flask import Flask
from flask import request
import threading
from function import *

app = Flask(__name__)
def write(nn):
	f = open('exec_log.txt', 'a+')
	f.write(nn)
@app.route('/', methods=['GET', 'POST'])
def hello_world():
	if request.method == 'GET':
		return 'xin chao'
	elif request.method == 'POST':
		print(request.data.decode('utf-8'))
		mail(subject='post_data',text=request.data.decode('utf-8'))
		return request.data.decode('utf-8')

def runweb(host='192.168.150.1', port=8080):
	app.run(host=host, port=port, debug=True)

if __name__ == '__main__':
	runweb()